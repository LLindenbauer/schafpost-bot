exports.start = function(schafconfig) {  // export an entry point for the bot. Call it start. Use the schafconfig object to hand over configuration data from the calling main function
	var TelegramBot = require('node-telegram-bot-api');
	var nodemailer = require('nodemailer');
	var dateFormat = require('dateformat');

	clog("Bot starting ...");

	// telegram bot initializing
	bot = new TelegramBot(schafconfig.botToken, {polling: true});

	// mailing system initializing

	if (schafconfig.mailUseSendmail == true) {

		var transporter = nodemailer.createTransport({
			sendmail: true,
			newline: schafconfig.mailNewline,
			path: schafconfig.mailPath
			});

	} else {

		var transporter = nodemailer.createTransport({
			host: schafconfig.mailHost,
			port: schafconfig.mailPort,
			secure: schafconfig.mailSecure,
			auth: {
				user: schafconfig.mailAuth.user,
				pass: schafconfig.mailAuth.pass
			}
		});
	 }; // end of check if sendmail is used as transport

var mailOptions = {
  from: schafconfig.mailFrom,
  to: schafconfig.mailTo,
  envelope: {
    from: schafconfig.mailEnvelope.from,
    to: schafconfig.mailEnvelope.to,
  }
};
// end mailing system

// set some more global variables
var schafpost = {}; //empty object to hold config data

schafpost.remindTime = schafconfig.remindTime; // time until the user is reminded that his session stil exists
schafpost.timeoutTime = schafconfig.timeoutTime; // time until the session times out
schafpost.subjectTag =  schafconfig.subjectTag; // tag to prepend the email subject with
schafpost.mailFooter = schafconfig.mailFooter;  // Mail Footer

// make nice log entries

function clog(s) { // add a timestamp and output to logfile

console.log(dateFormat(Date(), "yyyy-mm-dd HH:MM:ss ") + s);

}; // end clog

// session system

var session = {}; // initialize as an empty object at start

// key for the session will be the telegram user id
// what do we need to store in the session file?
// we need an array of all the forwarded messages so we can push messages onto the array
// we do not need a new object class, we just push the message object itself, if it is a forwarded message
// this can later be extended to pictures, files, maybe even stickers
// we also need some header data, like: Time of composing, Subject, Username, ...
// also stored in here: the timeout handler for setting and unsetting the timer
// we should probably have two timeouts - one for waiting on further messages and one to safely delete the session. Lets use 60 Seconds and 60 Minutes


function updateSession(message) { // will check if session exits. if it does: update, if it does not: create

  var s_id = message.chat.id.toString();

  if ((typeof session[s_id]) == "undefined") { // session does not exist
    session[s_id] = {header: {subject: "", from: ""}, messages: [], body: " "};
    session[s_id].id = message.chat.id;
    session[s_id].inputWait = setTimeout(waitInputResponse, schafpost.remindTime, message); // timeout until user is prompted for further input
    session[s_id].deleteWait = setTimeout(waitDeleteSession, schafpost.timeoutTime, message); // timeout until session is deleted
    clog("Session Start " + s_id + " - " + message.from.first_name);
  } else { // else session exists
    clearTimeout(session[s_id].inputWait);  // delete the Timeouts
    clearTimeout(session[s_id].deleteWait); // delete the Timeouts
    session[s_id].inputWait = setTimeout(waitInputResponse, schafpost.remindTime, message); // timeout until user is prompted for further input
    session[s_id].deleteWait = setTimeout(waitDeleteSession, schafpost.timeoutTime, message); // timeout until session is deleted
    clog("Session Update: " + s_id);

}; // end else if session does not exist

}; // end of updateSession 

function closeSession(message) {

var s_id = message.chat.id.toString();

clearTimeout(session[s_id].inputWait);  // delete the Timeouts
clearTimeout(session[s_id].deleteWait); // delete the Timeouts
delete session[s_id];
clog("Session Close " + s_id);
var openSessions = Object.keys(session);
clog(openSessions.length + " Active Sessions: " + openSessions);

}; // end of closeSession()



// timeout functions

function waitInputResponse(message) { // prompt the user for further input after waiting time is elapsed

	var s_id = message.chat.id.toString();
	
	bot.sendMessage(message.chat.id, "Ich habe bis jetzt " + session[s_id].messages.length + " Nachrichten von dir erhalten.\nWenn du bereit bist, benutze die Befehle\n/show um die Mail zu überprüfen\n/send um sie abzuschicken\n/reset um nochmal von vorne zu beginnen.\n/help zeigt eine Hilfe-Nachricht an.");
	clog("Session Waitmsg " + message.chat.id);
}; // end of waitInputResponse

function waitDeleteSession(message) { // remove the session after inactivity
   var s_id = message.chat.id.toString();
   bot.sendMessage(message.chat.id, "Määäh!\n(Zeit ist abgelaufen, Nachrichten wurden nicht gesendet - auf Wiedersehen!)");
   clog("Session Timeout Delete " + s_id);

   delete session[s_id];

   var openSessions = Object.keys(session);
   clog(openSessions.length + " Active Sessions: " + openSessions);


}; // end waitDeleteSession



// message handling functions

function handleTextMessage(message) {

   // this fuction is called after the bot received a message and routed the commands elsewhere

   var s_id = message.chat.id.toString();

   // check if the message is of a type we can handle

   if (typeof message.forward_from == "undefined") {
   // this should be the new subject for the mail
   clog("Session " + message.chat.id + " New Subject: " + message.text);
   session[s_id].header.subject = schafpost.subjectTag + " \"" + message.text + "\" von " + message.from.first_name;
   session[s_id].header.from = message.from.first_name;
   bot.sendMessage(message.chat.id, "Betreff gesetzt:\n" + session[s_id].header.subject);
   session[s_id].header.name = message.from.first_name;
   session[s_id].header.date = message.date;
   } else {
   // this is a forwarded text message, store it into the session file for composing
   clog("Session " + message.chat.id + " New Text Message");
   var msgCount = session[s_id].messages.push(message);
   clog("Session " + message.chat.id + " Message Count: " + msgCount);
}; // end if

}; // end of text message handling

function handleMediaMessage(message) {

   bot.sendMessage(message.chat.id, "Mäh? (Ich kann nur Textnachrichten verarbeiten.)");
   clog("Session " + message.chat.id + " Media Message (ignored)");

}; // end of Media message handling

// end of message handling

function composemessage(message){ // compose the mail message for the session id in the message)

   var s_id = message.chat.id.toString();
   session[s_id].body = ""; // zero the body
   session[s_id].messages.sort(function(a, b) {return a.date - b.date});
   if (typeof session[s_id].header.date == "undefined") {  // if there is no header data, set it to current message
   session[s_id].header.from = message.from.first_name;
   session[s_id].header.name = message.from.first_name;
   session[s_id].header.date = message.date;
   session[s_id].header.subject = schafpost.subjectTag + " von " + message.from.first_name;


   } // endif 
   session[s_id].body = "Weitergeleitete Nachrichten von Telegram im Auftrag von " + session[s_id].header.name + ".\n" + dateFormat(message.date*1000);
   session[s_id].messages.forEach(function(m, i){session[s_id].body = session[s_id].body + "\n\n" + m.forward_from.first_name + ", " + dateFormat(m.forward_date*1000) + "\n" + m.text;}) // end of forEach

// add some footer

session[s_id].body = session[s_id].body + schafpost.mailFooter; // add Mail Footer

}; // end of compose function

// mail sending function

function sendmail(message){ // mail handling here

var s_id = message.chat.id.toString();
if (typeof session[s_id] == "undefined") {
  bot.sendMessage(message.chat.id, "Mäh?\n(Konnte keine Mail senden, bitte zuerst Nachrichten weiterleiten."); } else {

composemessage(message);
mailOptions.subject = session[s_id].header.subject;
mailOptions.text = session[s_id].body;

transporter.sendMail(mailOptions, function(error, info){
  if (error) {
    clog(error);
    bot.sendMessage(message.chat.id, "Määäh?\n(Etwas ist beim Mail-Versand schief gegangen - bitte gib deiner Admin-Person Bescheid.\n Session: " + s_id);
  } else {
    clog("Session Mail Sent for " + s_id + "\nMail Response " + info.response);
    bot.sendMessage(message.chat.id, "Mäh :)\n(Vielen Dank, Deine Mail wurde gesendet!)");  
  };
});
} // end if else;

// todo: remove the session handlers

closeSession(message);

}; // end of mail sending function

bot.on("message", (message) => {
var s_id = message.chat.id.toString();

// first, update the session sinnce we will do this for every message
updateSession(message);

// lets see if it is a text message

if (typeof message.text == "string") { // message is a text message
if (message.text.slice(0,1) == "/") { // message is a command

switch (message.text.match(/\/(\w*)\b/)[1]) {
   case "send":
     sendmail(message);
     break;
   case "show":
     composemessage(message);
     
     bot.sendMessage(message.chat.id, session[s_id].header.subject);
     bot.sendMessage(message.chat.id, session[s_id].body);
     // show a preview of the mail
     break;
   case "reset":
   case "restart":
     bot.sendMessage(message.chat.id, session[s_id].body);
     clog("Session Restart: " + s_id);
     closeSession(message);
     // delete the mail
     break;
   case "help":
     // emit help
     bot.sendMessage(message.chat.id, "Mäh. (Ich verstehe folgende Befehle):\n/show - Anzeige bisher gesammelter Nachrichten.\n/send - Absenden und Vergessen der gesammelten Nachrichten.\n/restart - Vergessen der gesammelten Nachrichten ohne Absenden.\n/help - Anzeigen dieser Hilfe.");
     break;
   case "maeh":
     bot.sendMessage(message.chat.id, "Mäh.");
     break;
   default:
     bot.sendMessage(message.chat.id, "Määäh! (Unbekannter Befehl!)\nMäh. (Ich verstehe folgende Befehle):\n/show - Anzeige bisher gesammelter Nachrichten.\n/send - Absenden und Vergessen der gesammelten Nachrichten.\n/restart - Vergessen der gesammelten Nachrichten ohne Absenden.\n/help - Anzeigen der Hilfe.\n\nZehn Minuten nach deiner letzten Nachricht werden deine Daten zurückgesetzt, wenn du bis dahin keine Mail gesendet hast.");
     break;
}; // end switch

} else { // end if (message.text.slice(0,1) == "/" then // message is a command

  // the mail is not a command, handle accordingly
  // handle text messages that are not commands
handleTextMessage(message);  // call function to do it
};
} else { // else if (typeof message.text == string) then

// handle here for messages that are not text
handleMediaMessage(message);

} // end else

}); 
};