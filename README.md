# Schafpost

This is a Bot for Telegram based on node-telegram-bot-api ( https://github.com/yagop/node-telegram-bot-api )

The bot also requires the nodemailer and dateformat packages

## Running the bot

After installing with `npm install schafpost`, create a node.js-file and use `bot = require('schafbot')`.

Create a configuration object and pass it on to the `bot.start()` function of the module. The object needs the following properties (or the bot will crash):

* botToken: Telegram Bot token, see https://core.telegram.org/bots
* remindTime and timeoutTime: Values in milliseconds to wait for until a reminder is sent to the user and the session times out, respectively
* subjectTag: string that is prepended to the Subject of the EMail
* mailFooter: string that is attached below the mail
* mailFrom, mailTo, and mailEnvelope{} are the corresponding values for the nodemailer component, see https://nodemailer.com for possible values.
* Depending on the nodemailer transport you want to use, either:
	* mailHost, mailPort, mailSecure and mailAuth for using SMTP transport or
	* mailUseSendmail, mailNewline and mailPath to use sendmail


## Using sendmail as the mail transport

If you want to use this bot with a sendmail installation, set ```schafconfig.mailUseSendmail = true``` and set the ```schafconfig.mailPath``` and ```schafconfig.mailNewline``` to appropriate values. See nodemailer.com/transports/sendmail for further informations.

Setting the ```mailUseSendmail``` variable to true makes this bot ignore any SMPT configuration.

## Usage Example
```
bot = require('schafpost');

var schafconfig = {
	botToken: "TELEGRAM-BOT-TOKEN", // refer to the telegram bot faq on how to get your bot token
        mailHost: "localhost",
        mailPort: "465",
        mailSecure: true,
        mailAuth:
                {
                user: 'username@localhost',
                pass: 'PASSWORD'
                },
        mailFrom: 'username@localhost',
        mailTo: 'monitor@localhost',
        mailEnvelope:
                {
                from: 'User@localhost',
                to: 'monitor@localhost',
                },

        remindTime: 1000 * 15 * 1,  // time until the user is reminded that their session still exists
        timeoutTime: 1000 * 60 * 5, // time until the session times out
        subjectTag: "[Schafpost]", // tag to prepend the email subject with
        mailFooter: "\n\nE-Mail erstellt durch SchafPost.\nVisit me on www.npmjs.com/package/schafpost" // Mail Footer  
	};

bot.start(schafconfig);
```

## Usage Example Using Sendmail Transport

```
bot = require('schafpost');

var schafconfig = {
	botToken: "TELEGRAM-BOT-TOKEN", // refer to the telegram bot faq on how to get your bot token
        mailUseSendmail: true,
        mailNewline: "unix",
        mailPath: "/usr/sbin/sendmail",

        mailFrom: 'username@localhost',
        mailTo: 'monitor@localhost',
        mailEnvelope:
                {
                from: 'User@localhost',
                to: 'monitor@localhost',
                },

        remindTime: 1000 * 15 * 1,  // time until the user is reminded that his session still exists
        timeoutTime: 1000 * 60 * 5, // time until the session times out
        subjectTag: "[Schafpost]", // tag to prepend the email subject with
        mailFooter: "\n\nE-Mail erstellt durch SchafPost.\nVisit me on www.npmjs.com/package/schafpost" // Mail Footer  
	};

bot.start(schafconfig);
